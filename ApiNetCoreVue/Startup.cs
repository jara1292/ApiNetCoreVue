﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiNetCoreVue.Persistence;
using ApiNetCoreVue.Service;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

namespace ApiNetCoreVue
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            //registrar dependencia en nuestro StartUp para que este disponible para todo el proyecto.
            var connection = Configuration.GetConnectionString("Dev");
            services.AddDbContext<StudentDbContext>(options => options.UseSqlServer(connection));

            services.AddTransient<IStudentService, StudentService>();





            /*habilitar CORS para permitir realizar consultas AJAX a otro servidor que no pertenezca 
                    a nuestro dominio actual. 
            Para esto nos vamos a StartUp y creamos un Policy o una política.*/
            services.AddCors(options =>
            {
                options.AddPolicy("AllowSpecificOrigin", builder =>
                    builder.AllowAnyHeader()
                           .AllowAnyMethod()
                           .AllowAnyOrigin()
                );
            });



            services.AddMvc();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }


            //le decimos a NETCore que queremos usar la política creada.
            app.UseCors("AllowSpecificOrigin");


            //app.UseMvc();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
