﻿using ApiNetCoreVue.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiNetCoreVue.Persistence
{
    public class StudentDbContext: DbContext
    {
        public DbSet<Student> Student { get; set; }

        public StudentDbContext(DbContextOptions<StudentDbContext> options)
            : base(options)
        { }
    }
}
